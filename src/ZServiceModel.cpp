/*
 * File:   ZServiceModel.cpp
 * Author: tiennd
 *
 * Created on November 7, 2014, 5:36 PM
 */

#include "ZServiceModel.h"
#include "ServiceFactory.h"
#include "ExceptionServer.h"
#include <iostream>


namespace Up {
namespace Core {
namespace M2M {
namespace Group {

void ZServiceModel::traceFunction(const std::string& strFunc) {
#ifdef DLOG
    std::cout << strFunc << " is called\n";
#endif
}

void ZServiceModel::tracePagram(int num, ...) {
#ifdef DLOG
    va_list vl;
    va_start(vl, num);
    for (int i = 0; i < num; ++i) {
	std::cout << va_arg(vl, const char *);
    }
    std::cout << "\n";
#endif
}

ZServiceModel::ZServiceModel(Poco::SharedPtr<PersistentStorageType> aStorage)
: _storage(aStorage) {
}

ZServiceModel::ZServiceModel(const ZServiceModel& orig) {

}

ZServiceModel::~ZServiceModel() {

}

class create_visitor : public PersistentStorageType::data_visitor {
public:

    create_visitor(const M2M::Group::TValue &vl) : _vl(vl) {
    }

    virtual bool visit(const PersistentStorageType::TKey& key, PersistentStorageType::TValue& value) {
	if (value != model::ModelData::defVal) throw EXCEPTION_SERVER(ERROR_SERVER, ErrorServer::ERR_ITEM_EXIST);
	value.copyFrom(_vl);
	return true;
    }

private:
    const M2M::Group::TValue &_vl;
};

void ZServiceModel::create(ErrorCode& _return, const TKey tKey, const TValue& tValue) {

    // FIXME
    TRACE_FUNCTION;

    try {

	if (_storage.get() == NULL) throw EXCEPTION_SERVER_TYPE(ERROR_SERVER, ErrorServer::ERR_NULL_STORAGE);
	create_visitor visitor(tValue);
	_storage->visit(tKey, &visitor);
	_return.code = ErrorServer::ERR_NONE;

    } catch (const ExceptionServer& e) {

	_return.code = e.getError();
	_return.__set_message(e.getErrorMessage());

    } catch (...) {

	std::cerr << "Error unknown";
	ExceptionServer e = EXCEPTION_SERVER_TYPE(ERROR_SERVER, ErrorServer::ERR_UNKNOWN);
	_return.code = e.getError();
	_return.__set_message(e.getErrorMessage());
    }
}

class get_visitor : public PersistentStorageType::data_visitor {
public:

    get_visitor(TResult& return_)
    : _return(return_) {
    }

    virtual bool visit(const PersistentStorageType::TKey& key, PersistentStorageType::TValue& value) {
	if (value == model::ModelData::defVal) throw EXCEPTION_SERVER_TYPE(ERROR_SERVER, ErrorServer::ERR_ITEM_NOT_FOUND);
	auto &vl = _return.tValue;
	value.copyTo(vl);
	return false;
    }

private:
    TResult& _return;
};

void ZServiceModel::get(TResult& _return, const TKey tKey) {

    // FIXME
    TRACE_FUNCTION;

    try {
	if (_storage.get() == NULL) throw EXCEPTION_SERVER_TYPE(ERROR_SERVER, ErrorServer::ERR_NULL_STORAGE);
	get_visitor visitor(_return);
	_storage->visit(tKey, &visitor);

	_return.__isset.tValue = true;
	if (!_return.tValue.users.empty()) _return.tValue.__isset.users = true;
	if (!_return.tValue.doors.empty()) _return.tValue.__isset.doors = true;
	auto &error = _return.error;
	error.code = ErrorServer::ERR_NONE;

    } catch (const ExceptionServer& e) {

	auto &error = _return.error;
	error.code = e.getError();
	error.__set_message(e.getErrorMessage());

    } catch (...) {

	std::cerr << "Error unknown";
	ExceptionServer e = EXCEPTION_SERVER_TYPE(ERROR_SERVER, ErrorServer::ERR_UNKNOWN);
	auto &error = _return.error;
	error.code = e.getError();
	error.__set_message(e.getErrorMessage());
    }
}

class put_visitor : public PersistentStorageType::data_visitor {
public:

    put_visitor(const M2M::Group::TValue& vl) : _vl(vl) {
    }

    virtual bool visit(const PersistentStorageType::TKey& key, PersistentStorageType::TValue& value) {
	value.copyFrom(_vl);
	return true;
    }

private:
    const M2M::Group::TValue& _vl;
};

void ZServiceModel::put(ErrorCode& _return, const TKey tKey, const TValue& tValue) {
    // FIXME
    TRACE_FUNCTION;

    try {

	if (_storage.get() == NULL) throw EXCEPTION_SERVER_TYPE(ERROR_SERVER, ErrorServer::ERR_NULL_STORAGE);
	put_visitor visitor(tValue);
	_storage->visit(tKey, &visitor);
	_return.code = ErrorServer::ERR_NONE;

    } catch (const ExceptionServer& e) {
	_return.code = e.getError();
	_return.__set_message(e.getErrorMessage());

    } catch (...) {
	std::cerr << "Error unknown";
	ExceptionServer e = EXCEPTION_SERVER_TYPE(ERROR_SERVER, ErrorServer::ERR_UNKNOWN);
	_return.code = e.getError();
	_return.__set_message(e.getErrorMessage());
    }
}

void ZServiceModel::multiPut(ListError& _return, const ListValue& listValue) {

    // FIXME
    TRACE_FUNCTION;

    _return.error.code = ErrorServer::ERR_NONE;
    auto &mapList = listValue.groups;
    for (auto iter = mapList.begin(); iter != mapList.end(); ++iter) {
	auto &tKey = iter->first;
	auto &tValue = iter->second;
	auto &groups = _return.groups;
	put(groups[tKey], tKey, tValue);
    }

}

void ZServiceModel::remove(ErrorCode& _return, const TKey tKey) {
    // FIXME
    TRACE_FUNCTION;

    // TODO
}

void ZServiceModel::listGroup(ListTResult& _return, const int32_t start, const int32_t limit) {

    // FIXME
    TRACE_FUNCTION;

    try {

	/* get from 1 ....*/
	int begin = 0;
	if (start == 0) begin = 1;
	else begin = start;

	/* get start */
	TResult tResult;
	int length = ServiceFactory::lengthLimit;
	while (length-- > 0) {
	    get(tResult, begin);
	    if (tResult.error.code == ErrorServer::ERR_NONE) break;
	}
	if (length <= 0) {
	    _return.error.code = ErrorServer::ERR_NONE;
	    return;
	}

	/* insert to mapResult */
	std::map<TKey, TResult > mapResult;
	if (!tResult.tValue.users.empty()) tResult.tValue.__isset.users = true;
	if (!tResult.tValue.doors.empty()) tResult.tValue.__isset.doors = true;
	mapResult[begin] = tResult;
	mapResult[begin].__isset.tValue = true;


	/* get list */
	TResult result;
	for (int i = 0; i < limit; ++i) {
	    int index = begin + i;
	    if (!tResult.tValue.users.empty()) tResult.tValue.__isset.users = true;
	    if (!tResult.tValue.doors.empty()) tResult.tValue.__isset.doors = true;

	    int length = ServiceFactory::lengthLimit;
	    while (length-- > 0) {
		get(result, index);
		if (result.error.code == 0) break;
	    }
	    if (length <= 0) continue;
	    mapResult[index] = result;
	    mapResult[index].__isset.tValue = true;
	}

	_return.error.code = ErrorServer::ERR_NONE;
	_return.groups = mapResult;
	_return.__isset.groups = true;

    } catch (const ExceptionServer& e) {

	auto &error = _return.error;
	error.code = e.getError();
	error.__set_message(e.getErrorMessage());

    } catch (...) {

	ExceptionServer e = EXCEPTION_SERVER_TYPE(ERROR_SERVER, ErrorServer::ERR_UNKNOWN);
	auto &error = _return.error;
	error.code = e.getError();
	error.__set_message(e.getErrorMessage());
    }

}

void ZServiceModel::multiGet(ListTResult& _return, const TKeyList& tKeyList) {

    // FIXME
    TRACE_FUNCTION;

    _return.error.code = ErrorServer::ERR_NONE;
    for (TKeyList::const_iterator iter = tKeyList.begin(); iter != tKeyList.end(); ++iter) {
	auto &groups = _return.groups;
	auto &tKey = *iter;
	get(groups[tKey], tKey);
    }

    if (!_return.groups.empty()) _return.__isset.groups = true;

}











}
}
}
}


