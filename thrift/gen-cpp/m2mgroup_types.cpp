/**
 * Autogenerated by Thrift Compiler (0.9.1)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
#include "m2mgroup_types.h"

#include <algorithm>

namespace Up { namespace Core { namespace M2M { namespace Group {

int _kErrorServerValues[] = {
  ErrorServer::ERR_NONE,
  ErrorServer::ERR_ITEM_EXIST,
  ErrorServer::ERR_ITEM_NOT_FOUND,
  ErrorServer::ERR_NULL_STORAGE,
  ErrorServer::ERR_UNKNOWN
};
const char* _kErrorServerNames[] = {
  "ERR_NONE",
  "ERR_ITEM_EXIST",
  "ERR_ITEM_NOT_FOUND",
  "ERR_NULL_STORAGE",
  "ERR_UNKNOWN"
};
const std::map<int, const char*> _ErrorServer_VALUES_TO_NAMES(::apache::thrift::TEnumIterator(5, _kErrorServerValues, _kErrorServerNames), ::apache::thrift::TEnumIterator(-1, NULL, NULL));

const char* Group::ascii_fingerprint = "E0E9840E9367630E1FBEF1503F7549C9";
const uint8_t Group::binary_fingerprint[16] = {0xE0,0xE9,0x84,0x0E,0x93,0x67,0x63,0x0E,0x1F,0xBE,0xF1,0x50,0x3F,0x75,0x49,0xC9};

uint32_t Group::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->name);
          this->__isset.name = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->description);
          this->__isset.description = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 3:
        if (ftype == ::apache::thrift::protocol::T_LIST) {
          {
            this->users.clear();
            uint32_t _size0;
            ::apache::thrift::protocol::TType _etype3;
            xfer += iprot->readListBegin(_etype3, _size0);
            this->users.resize(_size0);
            uint32_t _i4;
            for (_i4 = 0; _i4 < _size0; ++_i4)
            {
              xfer += iprot->readI32(this->users[_i4]);
            }
            xfer += iprot->readListEnd();
          }
          this->__isset.users = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 4:
        if (ftype == ::apache::thrift::protocol::T_LIST) {
          {
            this->doors.clear();
            uint32_t _size5;
            ::apache::thrift::protocol::TType _etype8;
            xfer += iprot->readListBegin(_etype8, _size5);
            this->doors.resize(_size5);
            uint32_t _i9;
            for (_i9 = 0; _i9 < _size5; ++_i9)
            {
              xfer += iprot->readString(this->doors[_i9]);
            }
            xfer += iprot->readListEnd();
          }
          this->__isset.doors = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 5:
        if (ftype == ::apache::thrift::protocol::T_I32) {
          xfer += iprot->readI32(this->flags);
          this->__isset.flags = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 6:
        if (ftype == ::apache::thrift::protocol::T_I32) {
          xfer += iprot->readI32(this->createTime);
          this->__isset.createTime = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 7:
        if (ftype == ::apache::thrift::protocol::T_I32) {
          xfer += iprot->readI32(this->lastUpdate);
          this->__isset.lastUpdate = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t Group::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("Group");

  xfer += oprot->writeFieldBegin("name", ::apache::thrift::protocol::T_STRING, 1);
  xfer += oprot->writeString(this->name);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("description", ::apache::thrift::protocol::T_STRING, 2);
  xfer += oprot->writeString(this->description);
  xfer += oprot->writeFieldEnd();

  if (this->__isset.users) {
    xfer += oprot->writeFieldBegin("users", ::apache::thrift::protocol::T_LIST, 3);
    {
      xfer += oprot->writeListBegin(::apache::thrift::protocol::T_I32, static_cast<uint32_t>(this->users.size()));
      std::vector<int32_t> ::const_iterator _iter10;
      for (_iter10 = this->users.begin(); _iter10 != this->users.end(); ++_iter10)
      {
        xfer += oprot->writeI32((*_iter10));
      }
      xfer += oprot->writeListEnd();
    }
    xfer += oprot->writeFieldEnd();
  }
  if (this->__isset.doors) {
    xfer += oprot->writeFieldBegin("doors", ::apache::thrift::protocol::T_LIST, 4);
    {
      xfer += oprot->writeListBegin(::apache::thrift::protocol::T_STRING, static_cast<uint32_t>(this->doors.size()));
      std::vector<std::string> ::const_iterator _iter11;
      for (_iter11 = this->doors.begin(); _iter11 != this->doors.end(); ++_iter11)
      {
        xfer += oprot->writeString((*_iter11));
      }
      xfer += oprot->writeListEnd();
    }
    xfer += oprot->writeFieldEnd();
  }
  xfer += oprot->writeFieldBegin("flags", ::apache::thrift::protocol::T_I32, 5);
  xfer += oprot->writeI32(this->flags);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("createTime", ::apache::thrift::protocol::T_I32, 6);
  xfer += oprot->writeI32(this->createTime);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("lastUpdate", ::apache::thrift::protocol::T_I32, 7);
  xfer += oprot->writeI32(this->lastUpdate);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(Group &a, Group &b) {
  using ::std::swap;
  swap(a.name, b.name);
  swap(a.description, b.description);
  swap(a.users, b.users);
  swap(a.doors, b.doors);
  swap(a.flags, b.flags);
  swap(a.createTime, b.createTime);
  swap(a.lastUpdate, b.lastUpdate);
  swap(a.__isset, b.__isset);
}

const char* ErrorCode::ascii_fingerprint = "96705E9A3FD7B072319C71653E0DBB90";
const uint8_t ErrorCode::binary_fingerprint[16] = {0x96,0x70,0x5E,0x9A,0x3F,0xD7,0xB0,0x72,0x31,0x9C,0x71,0x65,0x3E,0x0D,0xBB,0x90};

uint32_t ErrorCode::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_I32) {
          xfer += iprot->readI32(this->code);
          this->__isset.code = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->message);
          this->__isset.message = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t ErrorCode::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("ErrorCode");

  xfer += oprot->writeFieldBegin("code", ::apache::thrift::protocol::T_I32, 1);
  xfer += oprot->writeI32(this->code);
  xfer += oprot->writeFieldEnd();

  if (this->__isset.message) {
    xfer += oprot->writeFieldBegin("message", ::apache::thrift::protocol::T_STRING, 2);
    xfer += oprot->writeString(this->message);
    xfer += oprot->writeFieldEnd();
  }
  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(ErrorCode &a, ErrorCode &b) {
  using ::std::swap;
  swap(a.code, b.code);
  swap(a.message, b.message);
  swap(a.__isset, b.__isset);
}

const char* TResult::ascii_fingerprint = "C96B63199B261C694DE5092CCC6083B4";
const uint8_t TResult::binary_fingerprint[16] = {0xC9,0x6B,0x63,0x19,0x9B,0x26,0x1C,0x69,0x4D,0xE5,0x09,0x2C,0xCC,0x60,0x83,0xB4};

uint32_t TResult::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_STRUCT) {
          xfer += this->error.read(iprot);
          this->__isset.error = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_STRUCT) {
          xfer += this->tValue.read(iprot);
          this->__isset.tValue = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t TResult::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("TResult");

  xfer += oprot->writeFieldBegin("error", ::apache::thrift::protocol::T_STRUCT, 1);
  xfer += this->error.write(oprot);
  xfer += oprot->writeFieldEnd();

  if (this->__isset.tValue) {
    xfer += oprot->writeFieldBegin("tValue", ::apache::thrift::protocol::T_STRUCT, 2);
    xfer += this->tValue.write(oprot);
    xfer += oprot->writeFieldEnd();
  }
  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(TResult &a, TResult &b) {
  using ::std::swap;
  swap(a.error, b.error);
  swap(a.tValue, b.tValue);
  swap(a.__isset, b.__isset);
}

const char* ListTResult::ascii_fingerprint = "26D6A3BDB76E20B6D40ABAD0A30366C3";
const uint8_t ListTResult::binary_fingerprint[16] = {0x26,0xD6,0xA3,0xBD,0xB7,0x6E,0x20,0xB6,0xD4,0x0A,0xBA,0xD0,0xA3,0x03,0x66,0xC3};

uint32_t ListTResult::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_STRUCT) {
          xfer += this->error.read(iprot);
          this->__isset.error = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_MAP) {
          {
            this->groups.clear();
            uint32_t _size12;
            ::apache::thrift::protocol::TType _ktype13;
            ::apache::thrift::protocol::TType _vtype14;
            xfer += iprot->readMapBegin(_ktype13, _vtype14, _size12);
            uint32_t _i16;
            for (_i16 = 0; _i16 < _size12; ++_i16)
            {
              TKey _key17;
              xfer += iprot->readI32(_key17);
              TResult& _val18 = this->groups[_key17];
              xfer += _val18.read(iprot);
            }
            xfer += iprot->readMapEnd();
          }
          this->__isset.groups = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t ListTResult::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("ListTResult");

  xfer += oprot->writeFieldBegin("error", ::apache::thrift::protocol::T_STRUCT, 1);
  xfer += this->error.write(oprot);
  xfer += oprot->writeFieldEnd();

  if (this->__isset.groups) {
    xfer += oprot->writeFieldBegin("groups", ::apache::thrift::protocol::T_MAP, 2);
    {
      xfer += oprot->writeMapBegin(::apache::thrift::protocol::T_I32, ::apache::thrift::protocol::T_STRUCT, static_cast<uint32_t>(this->groups.size()));
      std::map<TKey, TResult> ::const_iterator _iter19;
      for (_iter19 = this->groups.begin(); _iter19 != this->groups.end(); ++_iter19)
      {
        xfer += oprot->writeI32(_iter19->first);
        xfer += _iter19->second.write(oprot);
      }
      xfer += oprot->writeMapEnd();
    }
    xfer += oprot->writeFieldEnd();
  }
  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(ListTResult &a, ListTResult &b) {
  using ::std::swap;
  swap(a.error, b.error);
  swap(a.groups, b.groups);
  swap(a.__isset, b.__isset);
}

const char* ListError::ascii_fingerprint = "A58C3B9150CD27543C36FA14F6563D9D";
const uint8_t ListError::binary_fingerprint[16] = {0xA5,0x8C,0x3B,0x91,0x50,0xCD,0x27,0x54,0x3C,0x36,0xFA,0x14,0xF6,0x56,0x3D,0x9D};

uint32_t ListError::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_STRUCT) {
          xfer += this->error.read(iprot);
          this->__isset.error = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_MAP) {
          {
            this->groups.clear();
            uint32_t _size20;
            ::apache::thrift::protocol::TType _ktype21;
            ::apache::thrift::protocol::TType _vtype22;
            xfer += iprot->readMapBegin(_ktype21, _vtype22, _size20);
            uint32_t _i24;
            for (_i24 = 0; _i24 < _size20; ++_i24)
            {
              TKey _key25;
              xfer += iprot->readI32(_key25);
              ErrorCode& _val26 = this->groups[_key25];
              xfer += _val26.read(iprot);
            }
            xfer += iprot->readMapEnd();
          }
          this->__isset.groups = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t ListError::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("ListError");

  xfer += oprot->writeFieldBegin("error", ::apache::thrift::protocol::T_STRUCT, 1);
  xfer += this->error.write(oprot);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("groups", ::apache::thrift::protocol::T_MAP, 2);
  {
    xfer += oprot->writeMapBegin(::apache::thrift::protocol::T_I32, ::apache::thrift::protocol::T_STRUCT, static_cast<uint32_t>(this->groups.size()));
    std::map<TKey, ErrorCode> ::const_iterator _iter27;
    for (_iter27 = this->groups.begin(); _iter27 != this->groups.end(); ++_iter27)
    {
      xfer += oprot->writeI32(_iter27->first);
      xfer += _iter27->second.write(oprot);
    }
    xfer += oprot->writeMapEnd();
  }
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(ListError &a, ListError &b) {
  using ::std::swap;
  swap(a.error, b.error);
  swap(a.groups, b.groups);
  swap(a.__isset, b.__isset);
}

const char* ListValue::ascii_fingerprint = "C38801EC942B9A15EF5F3F927A45E9EB";
const uint8_t ListValue::binary_fingerprint[16] = {0xC3,0x88,0x01,0xEC,0x94,0x2B,0x9A,0x15,0xEF,0x5F,0x3F,0x92,0x7A,0x45,0xE9,0xEB};

uint32_t ListValue::read(::apache::thrift::protocol::TProtocol* iprot) {

  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;


  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_MAP) {
          {
            this->groups.clear();
            uint32_t _size28;
            ::apache::thrift::protocol::TType _ktype29;
            ::apache::thrift::protocol::TType _vtype30;
            xfer += iprot->readMapBegin(_ktype29, _vtype30, _size28);
            uint32_t _i32;
            for (_i32 = 0; _i32 < _size28; ++_i32)
            {
              TKey _key33;
              xfer += iprot->readI32(_key33);
              TValue& _val34 = this->groups[_key33];
              xfer += _val34.read(iprot);
            }
            xfer += iprot->readMapEnd();
          }
          this->__isset.groups = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  return xfer;
}

uint32_t ListValue::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  xfer += oprot->writeStructBegin("ListValue");

  xfer += oprot->writeFieldBegin("groups", ::apache::thrift::protocol::T_MAP, 1);
  {
    xfer += oprot->writeMapBegin(::apache::thrift::protocol::T_I32, ::apache::thrift::protocol::T_STRUCT, static_cast<uint32_t>(this->groups.size()));
    std::map<TKey, TValue> ::const_iterator _iter35;
    for (_iter35 = this->groups.begin(); _iter35 != this->groups.end(); ++_iter35)
    {
      xfer += oprot->writeI32(_iter35->first);
      xfer += _iter35->second.write(oprot);
    }
    xfer += oprot->writeMapEnd();
  }
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(ListValue &a, ListValue &b) {
  using ::std::swap;
  swap(a.groups, b.groups);
  swap(a.__isset, b.__isset);
}

}}}} // namespace
