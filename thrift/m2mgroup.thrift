namespace java vng.up.core.m2m.group
namespace cpp Up.Core.M2M.Group

enum ErrorServer
{
    ERR_NONE = 0,
    ERR_ITEM_EXIST,
    ERR_ITEM_NOT_FOUND,
    ERR_NULL_STORAGE,
    ERR_UNKNOWN
}


struct Group
{
	1: string name,	
	2: string description,
	3: optional list<i32> users,
	4: optional list<string> doors,
	5: i32 flags = 0,
	6: i32 createTime,
	7: i32 lastUpdate
}

typedef i32 TKey
typedef Group TValue
typedef list<i32> TKeyList


struct ErrorCode
{
	1: i32 code,
	2: optional string message
}

struct TResult
{
	1: ErrorCode error,
	2: optional TValue tValue;
}

struct ListTResult
{
	1: ErrorCode error,
	2: optional map<TKey, TResult> groups
}


struct ListError
{
	1: ErrorCode error,
	2: map<TKey, ErrorCode> groups
}

struct ListValue
{
	1: map< TKey,TValue> groups
}


service M2MGroupService
{
	ErrorCode create(1: TKey tKey, 2: TValue tValue),
	TResult get(1: TKey tKey),
	ListTResult multiGet(1: TKeyList tKeyList),
	ErrorCode put(1: TKey tKey, 2: TValue tValue),
	ListError multiPut(1: ListValue listValue),
	ErrorCode remove(1: TKey tKey),
	ListTResult listGroup(1: i32 start, 2: i32 limit )		
}
