/*
 * File:   ExceptionServer.h
 * Author: tiennd
 *
 * Created on December 9, 2014, 3:25 PM
 */

#ifndef M2MEXCEPTION_H
#define	M2MEXCEPTION_H


#include <exception>
#include <iostream>
#include <Poco/SharedPtr.h>
#include <Poco/NumberFormatter.h>
#include "m2mgroup_types.h"

class ExceptionServer : public std::exception {
public:

    /* forward error */
    ExceptionServer(int type, const std::string& message, int line, const std::string& file)
    : _errorCode(type), _errorMessage(message), _line(line), _file(file) {
    }

    /* error by server no errorMessage */
    ExceptionServer(const std::string& nameServer, int type, int line, const std::string& file)
    : _errorCode(type), _line(line), _file(file) {

        using namespace Up::Core::M2M::Group;

        switch (type) {

            case ErrorServer::ERR_ITEM_EXIST: _errorMessage = "item exist by " + nameServer;
                return;

            case ErrorServer::ERR_ITEM_NOT_FOUND: _errorMessage = "Item not found by " + nameServer;
                return;

            case ErrorServer::ERR_NULL_STORAGE: _errorMessage = " Null Storage by " + nameServer;
                return;

            case ErrorServer::ERR_UNKNOWN: _errorMessage = " Error unknown Server by " + nameServer;
                return;

            default: _errorMessage = "Unknown error by " + nameServer;
        }

    }

    virtual ~ExceptionServer() throw () {
    }

    int getError() const {
        return _errorCode;
    }

    std::string getErrorMessage() const {
        return _errorMessage;
    }

    int getLine() const {
        return _line;
    }

    std::string getFile() const {
        return _file;
    }


private:
    int _errorCode;
    std::string _errorMessage;
    int _line;
    std::string _file;
};

/* forward error */
#define EXCEPTION_SERVER(type, message) ExceptionServer(type, message,  __LINE__, __FILE__ )

/* error by server no errorMessage */
#define EXCEPTION_SERVER_TYPE(nameServer, type) ExceptionServer(std::string(nameServer), type, __LINE__, __FILE__ )

/* error connection */
#define EXCEPTION_SERVER_CONNECTION(nameServer) ExceptionServer( std::string(nameServer), __LINE__, __FILE__ )


#define ERROR_SERVER "m2mgroupservice"

#endif	/* I64DATAEXCEPTION_H */

