#pragma once

#include "ZServiceModel.h"
#include "ServiceThriftHandlerBaseT.h"
#include "M2MGroupService.h"
#include "ServiceFactory.h"

namespace Up {
    namespace Core {
	namespace M2M {
	    namespace Group {

		class ServiceHandler : public TServiceThriftHandlerBaseT<ZServiceModel,
		M2MGroupServiceIf, M2MGroupServiceProcessor,
		::apache::thrift::protocol::TCompactProtocolFactory > {
		public:
		    typedef TServiceThriftHandlerBaseT<ZServiceModel,
		    M2MGroupServiceIf, M2MGroupServiceProcessor,
		    ::apache::thrift::protocol::TCompactProtocolFactory > _Base;

		    ServiceHandler(Poco::SharedPtr<ZServiceModel> aModel) : _Base(aModel) {
		    };

		public:

		    virtual void create(ErrorCode& _return, const TKey tKey, const TValue& tValue) {
			if (m_pmodel) m_pmodel->create(_return, tKey, tValue);
		    }

		    virtual void get(TResult& _return, const TKey tKey) {
			if (m_pmodel) m_pmodel->get(_return, tKey);
		    }

		    virtual void multiGet(ListTResult& _return, const TKeyList& tKeyList) {
			if (m_pmodel) m_pmodel->multiGet(_return, tKeyList);
		    }

		    virtual void put(ErrorCode& _return, const TKey tKey, const TValue& tValue) {
			if (m_pmodel) m_pmodel->put(_return, tKey, tValue);
		    }

		    virtual void multiPut(ListError& _return, const ListValue& listValue) {
			if (m_pmodel) m_pmodel->multiPut(_return, listValue);
		    }

		    virtual void remove(ErrorCode& _return, const TKey tKey) {
			if (m_pmodel) m_pmodel->remove(_return, tKey);
		    }

		    virtual void listGroup(ListTResult& _return, const int32_t start, const int32_t limit) {
			if (m_pmodel) m_pmodel->listGroup(_return, start, limit);
		    }

		};

	    }
	}
    }
}
