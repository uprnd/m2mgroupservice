/*
 * File:   ZServiceModel.h
 * Author: tiennd
 *
 * Created on November 7, 2014, 5:36 PM
 */
#pragma once


#ifndef ZSERVICEMODEL_H
#define	ZSERVICEMODEL_H

#include "storagedef.h"


namespace Up {
    namespace Core {
	namespace M2M {
	    namespace Group {

		class ZServiceModel {
		public:

		    static void traceFunction(const std::string& strFunc);
		    static void tracePagram(int num, ...);

		    ZServiceModel(Poco::SharedPtr<PersistentStorageType> aStorage);
		    virtual ~ZServiceModel();

		    void create(ErrorCode& _return, const TKey tKey, const TValue& tValue);
		    void get(TResult& _return, const TKey tKey);
		    void multiGet(ListTResult& _return, const TKeyList& tKeyList);
		    void put(ErrorCode& _return, const TKey tKey, const TValue& tValue);
		    void multiPut(ListError& _return, const ListValue& listValue);
		    void remove(ErrorCode& _return, const TKey tKey);
		    void listGroup(ListTResult& _return, const int32_t start, const int32_t limit);

		private:
		    ZServiceModel(const ZServiceModel& orig);
		    Poco::SharedPtr<PersistentStorageType> _storage;

		};

#define TRACE_FUNCTION ZServiceModel::traceFunction(__FUNCTION__)

	    }
	}
    }
}

#endif	/* ZSERVICEMODEL_H */

